#Copyright 2020 Ben Belland

Your credentials should be exported as environment variables
in the following format. The SID and TOKEN are estimated lengths only.


export PATHCREDS="/home/example/go/src/gitlab.com/entropy-/supercut/super/"
declare -x URLCREDS="www.example.com"
export CHAINCREDS="/etc/letsencrypt/live/www.example.com/fullchain.pem"
export CERTCREDS="/etc/letsencrypt/live/www.example.com/privkey.pem"
export TWILIO_ACCOUNT_SID=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
export TWILIO_AUTH_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
export TWILIO_PHONE_NUMBER=XXXXXXXXXXX
export TO_PHONE_NUMBER=XXXXXXXXXXX
export CREDS="USERNAME:PASSWORD"


Setting them in a bash script only works if the script then launches 
the binary. As it is not designed that way this is the best way
to get your credentials in to the program
