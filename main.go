package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
    "github.com/streadway/amqp"
    "log"
    "strconv"
    twilio "github.com/twilio/twilio-go"
    openapi "github.com/twilio/twilio-go/rest/api/v2010"
    gsm "github.com/nyaruka/gocommon/gsm7"
    "github.com/gin-gonic/gin"
	"github.com/microcosm-cc/bluemonday"
)
/*
 * In the form of
 * PATHCREDS="/home/example/go/src/gitlab.com/entropy-/supercut/super/"
 * URLCREDS="www.example.com"
 * CHAINCREDS="/etc/letsencrypt/live/www.example.com/fullchain.pem"
 * CERTCREDS="/etc/letsencrypt/live/www.example.com/privkey.pem"
 *  PATH := os.Getenv("PATHCREDS")
 *  URL := os.Getenv("URLCREDS")
 *  CHAIN := os.Getenv("CHAINCREDS")
 *  CERT := os.Getenv("CERTCREDS")

 *
 *
 */

type Entry struct {
    Vendor string
    Amount float64
    Note string
    Category string
}

func failOnError(err error, msg string) {
        if err != nil {
                log.Fatalf("%s: %s", msg, err)
        }
}

var entry Entry

func main() {
	fmt.Println("Doot")
    sendSMS()
    PATH := os.Getenv("PATHCREDS")
    CHAIN := os.Getenv("CHAINCREDS")
    CERT := os.Getenv("CERTCREDS")
	server := gin.Default()
    server.POST("/mess", coord)
	server.GET("/", index)
	server.GET("/home", index)
	server.NoRoute(index)
	server.Static("/assets", PATH)

	//autotls.Run(server, URL)
	server.RunTLS(":443", CHAIN, CERT)
	//server.Run(":80")
}


func emit(body string) {
        conn, err := amqp.Dial("amqp://"+os.Getenv("CREDS")+"@biggest.dumpster.world:5672/")
        failOnError(err, "Failed to connect to RabbitMQ")
        defer conn.Close()

        ch, err := conn.Channel()
        failOnError(err, "Failed to open a channel")
        defer ch.Close()

        err = ch.ExchangeDeclare(
                "logs_topic", // name
                "topic",      // type
                true,         // durable
                false,        // auto-deleted
                false,        // internal
                false,        // no-wait
                nil,          // arguments
        )
        failOnError(err, "Failed to declare an exchange")

        //body := ""//change this to a processed string
        err = ch.Publish(
                "logs_topic",          // exchange
                "trout", // routing key
                false, // mandatory
                false, // immediate
                amqp.Publishing{
                        ContentType: "text/plain",
                        Body:        []byte(body),
                })
        failOnError(err, "Failed to publish a message")

        log.Printf(" [x] Sent %s", body)
}


func getIt(c *gin.Context) {
	policy := bluemonday.UGCPolicy()
	policy.Sanitize("no")
}
func processString(inputs map[string]string, s string) string {
	output := ""
	//r := len(strings.Split(s, "::"))
	v := strings.Split(s, "::")
	output += v[0]

	output += inputs["URL"]
	output += v[2]
	output += inputs["URI"]
	output += v[4]

	return output
}

func populate(uri string, url string) string {
	links := `<div class="container">
      <a href="::URL::">
      <div class="picture">
        <img class="picture" src="::URI::"></img>
      </a><div class="text">
        This website, it is written in Golang, serves http currently and this is
        a thumbnail embeddable link. And now it is a template action to be applied.
        </div>
    </div>
</div>`

	payloadAlpha := make(map[string]string, 35)

	payloadAlpha["URL"] = url
	payloadAlpha["URI"] = uri

	stringPayload := processString(payloadAlpha, links)

	return stringPayload

}

func coord(c *gin.Context) {

    val, err := ioutil.ReadAll(c.Request.Body)
    if err != nil {
            panic(err)
    }

    //fmt.Println(string(val))
    splitVal := strings.Split(string(val), "&")

    for i := range splitVal {
            if strings.Contains(splitVal[i], "Body") {
                body := strings.Split(splitVal[i], "=")
                fmt.Println(body[1:])
                //<== "New entry"
                if strings.Contains(body[1], "New+entry") {
                    SMS("New entry it is, what was the vendor?")

                    entry.Vendor = "Pizza Hut"
                    entry.Amount = 999.99
                    entry.Note = "placeholder"
                    entry.Category = "blank"
                }else if entry.Vendor == "Pizza Hut" {
                    entry.Vendor = gsm.ReplaceSubstitutions(body[1])

                    /*splitBody := strings.Split(body[1], "+")
                    processedBody := ""
                    for i := range splitBody {
                            processedBody += splitBody[i] + " "
                    }
                    entry.Vendor = processedBody[:len(processedBody)]
                    */fmt.Println("Entry :"+entry.Vendor)
                    SMS("What was the amount?")

                }else if entry.Amount == 999.99 {
                    amt, err := strconv.ParseFloat(body[1], 64)
                    if err != nil {
                        fmt.Println("Invalid amount")
                    }else {
                        entry.Amount = amt
                        fmt.Println("Amount :" + body[1])
                        SMS("What category?")
                    }
                }else if entry.Category == "blank" {
                    entry.Category = gsm.ReplaceSubstitutions(body[1])

                    /*splitBody := strings.Split(body[1], "+")
                    processedBody := ""
                    for i := range splitBody {
                            processedBody += splitBody[i] + " "
                    }
                    entry.Category = processedBody[:len(processedBody)]
                    */fmt.Println("Category :"+entry.Category)
                    SMS("Any notes?")
                }else if entry.Note == "placeholder" {
                    entry.Note = gsm.ReplaceSubstitutions(body[1])

                    /*splitBody := strings.Split(body[1], "+")
                    processedBody := ""
                    for i := range splitBody {
                            processedBody += splitBody[i] + " "
                    }
                    entry.Note = processedBody[:len(processedBody)]
                    */fmt.Println("Note :"+entry.Note)
                    SMS("Done!")
                    amt := strconv.FormatFloat(entry.Amount, 'f', -1, 64)
                    SMS("Vendor:"+entry.Vendor+"\nAmount:"+amt+"\nCategory:"+entry.Category+"\nNotes:"+entry.Note)
                }else {
                        fmt.Println("Invalid option")

                    break
                }
            }
    }

    if entry.Vendor != "Pizza Hut" && len(entry.Vendor) > 1 && entry.Amount != 999.99 && entry.Note != "placeholder" && entry.Category != "blank"{
        policy := bluemonday.UGCPolicy()
        v := strconv.FormatFloat(entry.Amount, 'f', -1, 64)


        value := policy.Sanitize(entry.Vendor)+":"+v+":"+policy.Sanitize(entry.Category)+":"+policy.Sanitize(entry.Note)

        emit(value)
    }

}
func sendSMS() {
        client := twilio.NewRestClient()
        paramsAPI := openapi.CreateMessageParams{}
        paramsAPI.SetTo(os.Getenv("TO_PHONE_NUMBER"))
        paramsAPI.SetFrom(os.Getenv("TWILIO_PHONE_NUMBER"))
        //this should only be run once on running of the binary
        paramsAPI.SetBody("The password is TROUT")

        _, err := client.ApiV2010.CreateMessage(&paramsAPI)
        if err != nil {
                panic(err)
        }else {
                fmt.Println("SMS sent!")
        }
}

func SMSEntry(body string) {
    SMS("New entry it is, what was the vendor?")
    //<== "Pizza Hut"
    //var entry Entry{}
    //entry.vendor = "Pizza Hut"
    //confirmCorrectFunc()
    SMS("How much was it for?")
    //<== "24.59"
    //entry.amount = "24.59"
    //confirmCorrectFunc()
    SMS("Any notes?")
    //<== "Best lunch ever"
    //entry.note = "Best lunch ever"
    //confirmCorrectFunc()


}

func SMS(body string) {
        client := twilio.NewRestClient()
        paramsAPI := openapi.CreateMessageParams{}
        paramsAPI.SetTo(os.Getenv("TO_PHONE_NUMBER"))
        paramsAPI.SetFrom(os.Getenv("TWILIO_PHONE_NUMBER"))
        //this should only be run once on running of the binary
        paramsAPI.SetBody(body)

        _, err := client.ApiV2010.CreateMessage(&paramsAPI)
        if err != nil {
                panic(err)
        }else {
                fmt.Println("SMS sent!")
        }



}
func index(c *gin.Context) {
	output := ""
    PATH := os.Getenv("PATHCREDS")

	//Define your payloads
	pl := "https://gitlab.com/entro-pi/supercut"
	pi := "assets/img/cc.png"
	pbl := "https://gitlab.com/entro-pi/snowfone"
	pbi := "assets/img/knoife.png"

	//Format  your payloads
	stringPayload := populate(pi, pl)
	stringPayload += populate(pbi, pbl)

	dir, err := ioutil.ReadDir(PATH)
	if err != nil {
		panic(err)
	}
	for _, val := range dir {
		if val.Name() == "img" {
			file, err := os.Open(PATH + "/index_feader.html")
			if err != nil {
				panic(err)
			}

			payload, err := ioutil.ReadAll(file)
			if err != nil {
				panic(err)
			}
			c.Writer.Write(payload)
			c.Writer.Write([]byte(stringPayload))
			//c.File(PATH+"/index_feader.html")

			pictureDir, err := ioutil.ReadDir(PATH + "/img")
			if err != nil {
				panic(err)
			}

			for i, pic := range pictureDir {
				if len(pic.Name()) > 2 {
					//do thing!
				} else {
					continue
				}
				item := `<a href="assets/img/` + pic.Name() + `" data-caption="testing lightbox">

            <img src="assets/img/` + pic.Name() + `" width="39px" height="39px" style="border-radius: 50%;" alt="First image">

            </a>`
				item0 := `<div class="gallery" style="margin: 0 auto;">`
				end := false
				if i == 0 {
					output += item0
					end = true
				}
				output += item
				if end {
					output += `</div>`
				}
			}

			//c.Writer.Write([]byte(output))
			file, err = os.Open(PATH + "/index_hooter.html")
			if err != nil {
				panic(err)
			}

			payload, err = ioutil.ReadAll(file)
			if err != nil {
				panic(err)
			}
			c.Writer.Write(payload)
		}
	}
}
